var express = require('express')
, router = express.Router()
, master = require('../Models/master')
, security = require('../Models/security')
, mongoose = require('mongoose')
, datastore = require('../datastore')
, MongoClient = require('mongodb').MongoClient
, mongoConfig = require('../mongo.config')
, branch = require('../Database/branch')
, user = require('../Database/user')
, DatabaseClient = require('../Database/DatabaseClient') ;

router.get('/login', function(req, res) {
 res.render('./account/login.pug');
});

router.post('/login', function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    DatabaseClient.Connect(async (db) => {
      var u = await user.Login(db, email, password);
      if (u) {
        req.session.user = u;
        res.redirect('../home/index');
      } else {
        res.render('./account/login.pug', { error: true, message: 'Incorrect email or password!' });
      }
    });
});

router.get('/register', (req, res) => {
  DatabaseClient.Connect(async (db) => {
    var result = await branch.getBranches(db);
    res.render('./account/register.pug', { branches: result, warning: false, message: 'email already taken'  });
  });
});

router.post('/register', function(req, res) {
  DatabaseClient.Connect(async (db) => {
    var isEmailExists = await user.IsEmailExists(db, req.body.user.email);
    if (isEmailExists) {
     var result = await branch.getBranches(db);
      res.render('./account/register.pug', { branches: result, warning: isEmailExists, message: 'email already taken'  });
    }
    else {
      var u = {
        firstname: req.body.user.firstname,
        lastname: req.body.user.lastname,
        email: req.body.user.email,
        password: req.body.user.password,
        mobile: req.body.user.mobile,
        branch: +(req.body.user.branch)
      };
      await user.Register(db, u);
      res.redirect('./login');
    }
  });
});

module.exports = router
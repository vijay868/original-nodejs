var express = require('express')
, router = express.Router();

router.use('/account', require('./account'));
router.use('/home', require('./home'));
router.use('/config', require('./config'));

router.get('/', function(req, res) {
	res.render('./account/login.pug');
});



module.exports = router;
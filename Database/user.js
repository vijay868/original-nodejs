var q = require('q');

var user = {
    IsEmailExists: function (db, email) {
        var deferred = q.defer();
        db.collection('users')
            .find({ email: email })
            .toArray((mongoError, result) => {
                deferred.resolve(result.length > 0 ? true : false);
                if (mongoError) deferred.reject(mongoError);
            });
        return deferred.promise;
    },
    Register: function(db, user) {
        var deferred = q.defer();
        db.collection('users')
            .insertOne(user, (mongoError, insertOneWriteOpResult) => {
                deferred.resolve(true);
                if (mongoError) deferred.reject(mongoError);
            });
        return deferred.promise;
    },
    Login: function(db, email, password) {
        var deferred = q.defer();
        db.collection('users')
            .findOne({ email: email, password: password }, (mongoError, result) => {
                deferred.resolve(result);
                if (mongoError) deferred.reject(mongoError);
            });
        return deferred.promise;
    }
};

module.exports = user;
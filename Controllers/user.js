var q = require('q');

var user = {
    isUserExists: function (db, email) {
        var deferred = q.defer();
        db.collection('users')
            .find({ email: email })
            .toArray((mongoError, result) => {
                deferred.resolve(result.length > 0 ? true : false);
                if (mongoError) deferred.reject(mongoError);
            });
        return deferred.promise;
    }
};

module.exports = user;
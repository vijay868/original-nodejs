var mongoose = require('mongoose')
//, config = require('./config');

//mongoose.connect(config.get('mongo'));
//mongoose.connect('mongodb://localhost/original');

var BranchSchema = new mongoose.Schema({
     BranchID: Number,
     BranchCode: String, 
	 BranchName: String,
	 RegNo: String,
	 IsActive: Boolean,
	 CompanyCode: String,
     AddressID: String,
	 CreatedBy: { type: String, default: 'ADMIN' },
	 CreateOn: { type: Date, default: Date.now },
	 ModifiedBy: { type: String, default: 'ADMIN' },
	 ModifiedOn: { type: Date, default: Date.now }
});

BranchSchema.statics.GetBranches = function() {
  console.log('User.findByEmailAndPassword');
  return this
    .find({}, 'BranchID BranchCode BranchName')
    .exec();
}

var Branch = mongoose.model('MasterBranch', BranchSchema);
exports.Branch = Branch;

/*
var UserSchema = new mongoose.Schema({
     UserID: { type: String, unique: true, required: true },
	 UserName: { type: String, unique: true, required: true },
	 'Password': String,
	 IsActive: Boolean,
	 Email: { type: String, unique: true, required: true },
	 MobileNumber: { type: String, unique: true, required: true },
	 LogInStatus: Boolean,
	 LastLogInOn: { type: Date, default: Date.now },
	 RoleCode: { type: String, default: 'ADMIN' },
	 CreatedBy: { type: String, default: 'ADMIN' },
	 CreatedOn: { type: Date, default: Date.now },
	 ModifiedBy: { type: String, default: 'ADMIN' },
	 ModifiedOn: { type: Date, default: Date.now }
});

var User = mongoose.model('SecurityUsers', UserSchema);
exports.User = User;*/
//module.exports = mongoose.model('MasterBranch', BranchSchema);
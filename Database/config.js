var q = require('q');
var mongoClient = require('mongodb');

var config = {
    GetListByCategory: (db, category) => {
        var deferred = q.defer();
        db.collection('config')
            .find({ category: category })
            .toArray((mongoError, result) => {
                if (mongoError) deferred.reject(mongoError);
                deferred.resolve(result);
            });
        return deferred.promise;
    },
    GetDocByCodeAndCategory: function (db, category, code) {
        var deferred = q.defer();
        db.collection('config')
            .findOne(
                { category: category, lookupcode: code }, 
                (mongoError, result) => {
                if (mongoError) deferred.reject(mongoError);
                deferred.resolve(result);
            });
        return deferred.promise;
    },
    GetDocByCategoryAndId: function (db, category, id) {
        var deferred = q.defer();
        db.collection('config')
            .findOne(
                { category: category, _id: mongoClient.ObjectId(id) }, 
                (mongoError, result) => {
                if (mongoError) deferred.reject(mongoError);
                deferred.resolve(result);
            });
        return deferred.promise;
    },
    Save: function(db, config) {
        var deferred = q.defer();
        db.collection('config')
            .insertOne(config, (mongoError, insertOneWriteOpResult) => {
                if (mongoError) deferred.reject(mongoError);
                deferred.resolve(insertOneWriteOpResult.result.ok == 1 ? true : false);
            });
        return deferred.promise;
    },
    Update: function(db, config) {
        var deferred = q.defer();
        db.collection('config')
            .updateOne(
                { category: config.category, _id: mongoClient.ObjectId(config.id) },
                { $set: { description: config.description, lookupcode: config.lookupcode } },
                (mongoError, updateWriteOpResult) => {
                    if (mongoError) deferred.reject(mongoError)
                    deferred.resolve(updateWriteOpResult.result.ok == 1 ? true : false);                    
                });
        return deferred.promise;
    },
    Delete: function(db, category, code) {
        var deferred = q.defer();
        db.collection('config')
            .deleteOne(
                { category: category, lookupcode: code },
                (mongoError, deleteWriteOpResult) => {
                    if (mongoError) deferred.reject(mongoError);
                    deferred.resolve(deleteWriteOpResult.result.ok == 1 ? true : false);
                }
            ); 
        return deferred.promise;
    },
    DeleteById: function(db, category, id) {
        var deferred = q.defer();
        db.collection('config')
            .deleteOne(
                { category: category, _id: mongoClient.ObjectId(id) },
                (mongoError, deleteWriteOpResult) => {
                    if (mongoError) deferred.reject(mongoError);
                    deferred.resolve(deleteWriteOpResult.result.ok == 1 ? true : false);
                }
            ); 
        return deferred.promise;
    }
};

module.exports = config;

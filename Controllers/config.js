var express = require('express')
	, router = express.Router()
	, DatabaseClient = require('../Database/DatabaseClient')
	, configDbo = require('../database/config')
	, validateSession = require('../checksignin');

router.get('/:category/list', validateSession.checkSignIn, function (req, res) {
	DatabaseClient.Connect(async (db) => {
		var configArray = await configDbo.GetListByCategory(db, req.params.category);
		res.render(`./config/${req.params.category}list.pug`, { array: configArray });
	});
});

router.post('/:category/delete', function (req, res) {
	DatabaseClient.Connect(async (db) => {
		await configDbo.DeleteById(db, req.params.category, req.body.hdnDeleteId);
		res.redirect(`./list`);
	});
});

router.post('/:category/save', function (req, res) {
	var data = {
		lookupcode: req.body.lookupcode,
		description: req.body.description,
		category: req.body.category,
		status: true
	};
	DatabaseClient.Connect(async (db) => {
		var doc = null;
		if (req.body.id !== '')
			doc = await configDbo.GetDocByCategoryAndId(db, data.category, req.body.id);
		if (doc) {
			data.id = req.body.id;
			await configDbo.Update(db, data);
		} else {
			await configDbo.Save(db, data);
		}
		res.redirect(`./list`);
	});
});

/*
router.get('/uom/:lookupCode', function (req, res) {
	DatabaseClient.Connect(async (db) => {
		var doc = await configDbo.GetDocByCodeAndCategory(db, 'UOM', req.params.lookupCode)
		res.send(doc);
	});
});
*/
router.get('/:category/:id', function (req, res) {
	DatabaseClient.Connect(async (db) => {
		var doc = await configDbo.GetDocByCategoryAndId(db, req.params.category, req.params.id);
		res.send(doc);
	});
});

module.exports = router
var mongoose = require('mongoose');
//mongoose.createConnection('mongodb://localhost/original');

var UserSchema = new mongoose.Schema({
     UserID: String,
	 UserName: String,
	 'Password': String,
	 IsActive: { type: Boolean, default: true },
	 Email: String,
	 MobileNumber: String,
	 LogInStatus: { type: Boolean, default: true },
	 LastLogInOn: { type: Date, default: Date.now },
	 RoleCode: { type: String, default: 'ADMIN' },
	 CreatedBy: { type: String, default: 'ADMIN' },
	 CreatedOn: { type: Date, default: Date.now },
	 ModifiedBy: { type: String, default: 'ADMIN' },
	 ModifiedOn: { type: Date, default: Date.now }
});

var User = mongoose.model('SecurityUsers', UserSchema);
exports.User = User;
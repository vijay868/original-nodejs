var express = require('express')
, bodyParser = require('body-parser')
, http = require('http')
, util = require('util')
, passport = require('passport')
, session = require('express-session')
, cookieParser = require('cookie-parser')
, LocalStrategy = require('passport-local').Strategy;

var app = module.exports = express();
app.set('views', __dirname + '/views');
app.set('pug engine', 'pug');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(session({secret: '1234567890QWERTY'}));

app.use(require('./Controllers'));
app.listen(3000, function() {
	console.log('Listening on port 3000......');
});


/*
http://stackoverflow.com/questions/6331776/accessing-express-js-req-or-session-from-jade-template
*/

const mongoConfig = {
    topologyConfig: { 
        useNewUrlParser: true, 
        useUnifiedTopology: true 
    },
    uri: 'mongodb://localhost:27017/',
    database: 'original'
};

module.exports = mongoConfig;


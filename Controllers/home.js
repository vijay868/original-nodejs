var express = require('express')
, router = express.Router()
, validateSession = require('../checksignin');

router.get('/index', validateSession.checkSignIn, function(req, res) {
 res.render('./home/index.pug');
});

module.exports = router
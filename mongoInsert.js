var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/original';
mongoose.connect(dbURI);

var masterBranchSchema = new mongoose.Schema({
     BranchID: Number,
     BranchCode: String, 
	 BranchName: String,
	 RegNo: String,
	 IsActive: Boolean,
	 CompanyCode: String,
     AddressID: String,
	 CreatedBy: { type: String, default: 'ADMIN' },
	 CreateOn: { type: Date, default: Date.now },
	 ModifiedBy: { type: String, default: 'ADMIN' },
	 ModifiedOn: { type: Date, default: Date.now }
});

var branch = mongoose.model('MasterBranch', masterBranchSchema);

var branchOne = new branch({
     BranchID: 160,
     BranchCode: 'PATTANI', 
	 BranchName: 'PATTANI',
	 RegNo: '123',
	 IsActive: true,
	 CompanyCode: 'PATTANI',
     AddressID: ''
});

var branchTwo = new branch({
     BranchID: 170,
     BranchCode: 'PIYAROM', 
	 BranchName: 'PIYAROM',
	 RegNo: '123',
	 IsActive: true,
	 CompanyCode: 'PIYAROM',
     AddressID: ''
});

branchOne.save();
branchTwo.save();

mongoose.connection.close(function() {
	console.log('--Mongoose default connection closed');
});



var mongoose = require('mongoose')
  , debug = require('debug')('mongoose');

var dbURI = 'mongodb://localhost/original';
exports.connect=function (next) {
  mongoose.set('debug', true);
  mongoose.connect(dbURI, function(err) {
  	if (next) next();
  });
};

exports.shutdown=function() {
  mongoose.disconnect();
  mongoose.connection.close();
}
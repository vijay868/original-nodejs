var MongoClient = require('mongodb').MongoClient;
var mongoConfig = require('../mongo.config');

var DataBaseClient = {
    Connect: (exec) => {
        MongoClient.connect(
            mongoConfig.uri,
            mongoConfig.topologyConfig,
            async (mongoError, mongoClient) => {
                if (mongoError) throw mongoError;
                const db = mongoClient.db(mongoConfig.database);
                await exec(db);
                mongoClient.close();
            })
    },
    ConnectWithDbConfig: (config, exec) => {
        MongoClient.connect(
            config.uri,
            config.dbOptions,
            async (mongoError, mongoClient) => {
                if (mongoError) throw mongoError;
                const db = mongoClient.db(config.dbName);
                await exec(db);
                mongoClient.close();
            })
    }
};

module.exports = DataBaseClient;

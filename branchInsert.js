
var MongoClient = require('mongodb').MongoClient;
var uri = 'mongodb://localhost:27017/';

MongoClient.connect(uri, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}, (mongoError, mongoClient) => {
	console.log('connected to database');
	const db = mongoClient.db('original');
	console.log('reached upto db object');
	db.createCollection('branch', (mongoError, result) => {
		if (mongoError) console.log('my custom error' + mongoError);
		console.log('collection: branch created');
	});
	var branchOne = {
		BranchID: 160,
		BranchCode: 'PATTANI',
		BranchName: 'PATTANI',
		RegNo: '123',
		IsActive: true,
		CompanyCode: 'PATTANI',
		AddressID: ''
	};

	var branchTwo = {
		BranchID: 170,
		BranchCode: 'PIYAROM',
		BranchName: 'PIYAROM',
		RegNo: '123',
		IsActive: true,
		CompanyCode: 'PIYAROM',
		AddressID: ''
	};
	var obj = new Array();
	obj.push(branchOne);
	obj.push(branchTwo);
	db.collection('branch')
		.insertMany(obj, (mongoError, insertWriteOpResult) => {
			console.log(insertWriteOpResult);
			mongoClient.close();
		});
});



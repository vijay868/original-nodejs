var mongoose = require('mongoose');

var LookupSchema = mongoose.Schema({
	 LookupCode: { type: String, required: true, index: { unique: true, sparse: true } },
	 Description: String,
	 Description2: String,
	 Category: String,
	 Status: Boolean
});

 LookupSchema.statics.GetListByCategory = function(category, callback) {
  return this
	  .find({ Category: category })
	  .exec(callback);
 };

 LookupSchema.statics.GetDocumentByLookupCode = function(lookupCode, category, callback) {
  return this
	  .findOne({ LookupCode: lookupCode, Category: category })
	  .exec(callback);
 };

/*
 LookupSchema.statics.UpdateConfig = function() {
	 
 };
*/

 var Lookup = mongoose.model('ConfigLookup', LookupSchema);
 exports.Lookup = Lookup;
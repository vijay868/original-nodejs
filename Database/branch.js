var q = require('q');

var branch = {
    getBranches: function (db) {
        var deferred = q.defer();
        db.collection('branch')
            .find({}, { projection: { BranchID: 1, BranchCode: 1, BranchName: 1 } })
            .toArray((mongoError, result) => {
                deferred.resolve(result);
                if (mongoError) deferred.reject(mongoError);
            });
        return deferred.promise;
    }
};

module.exports = branch;
